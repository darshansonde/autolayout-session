//
//  OneViewController.m
//  autolayouttest
//
//  Created by Darshan Sonde on 12/10/14.
//  Copyright (c) 2014 Darshan Sonde. All rights reserved.
//

#import "OneViewController.h"

// ANCHORING

@interface OneViewController ()
@property (nonatomic, strong) UIButton *button1;
@end

@implementation OneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];//create the button and add as subview
    [self setupConstraints];
}


-(void) setupConstraints {
    //x = view.x * 1.0 + 0.0;
    NSLayoutConstraint *x = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeRight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view attribute:NSLayoutAttributeRight
                                                            multiplier:1.0 constant:0.0];
    [self.view addConstraint:x];
    
    NSLayoutConstraint *y = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0 constant:0.0];
    [self.view addConstraint:y];

}
















-(void) button1Action:(UIButton*)sender {
    [self.button1 setTitle:@"Hello Autolayout World!" forState:UIControlStateNormal];
}


















-(void) loadView {
    self.view = ({
        UIView *v = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        v.backgroundColor = [UIColor whiteColor];
        v.translatesAutoresizingMaskIntoConstraints = NO;
        v;
    });
}

-(void) setupUI {
    self.button1 = ({
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setTitle:@"Hello" forState:UIControlStateNormal];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor blueColor];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:b];
        b;
    });
}

@end
