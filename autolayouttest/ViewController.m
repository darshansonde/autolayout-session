//
//  ViewController.m
//  autolayouttest
//
//  Created by Darshan Sonde on 02/07/14.
//  Copyright (c) 2014 Darshan Sonde. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UIButton *button2;
@property (strong, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //By default, all UIKit- and AppKit-supplied views have a value of either NSLayoutPriorityDefaultHigh or NSLayoutPriorityDefaultLow.
    for (UIView *v in
         @[ [[UILabel alloc] init],
            [UIButton buttonWithType:UIButtonTypeCustom],
            [UIButton buttonWithType:UIButtonTypeInfoDark],
            [[UIView alloc] init],
            [[UISlider alloc] init],
            [[UITextView alloc] init],
            [[UITextField alloc] init],
            [[UIProgressView alloc] init]
            ]) {
        NSLog(@"%@ huggingPriority H=%f V=%f, compressionPriority H=%f V=%f",NSStringFromClass([v class]),
              [v contentHuggingPriorityForAxis:UILayoutConstraintAxisHorizontal],
              [v contentHuggingPriorityForAxis:UILayoutConstraintAxisVertical],
              [v contentCompressionResistancePriorityForAxis:UILayoutConstraintAxisHorizontal],
              [v contentCompressionResistancePriorityForAxis:UILayoutConstraintAxisVertical]);
    }
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.container.translatesAutoresizingMaskIntoConstraints = NO;
    self.label1.layer.borderColor  = [UIColor redColor].CGColor;
    self.label1.layer.borderWidth = 1.0f;
    self.label1.preferredMaxLayoutWidth = 200.0f;
    //self.con
    //NSLayoutConstraint *c =
//
//    self.button1 = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.button1.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.button1 setTitle:@"small asdf asdf  " forState:UIControlStateNormal];
//    self.button2 = [UIButton buttonWithType:UIButtonTypeCustom];
//        self.button2.translatesAutoresizingMaskIntoConstraints = NO;
//    [self.button2 setTitle:@"world" forState:UIControlStateNormal];
//
//    [self.view addSubview:self.button1];
//    
//    [self.view addSubview:self.button2];
//    

//    NSLayoutConstraint *b11 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeLeft
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.view attribute:NSLayoutAttributeLeft
//                                                         multiplier:1.0 constant:0.0];
//
//    NSLayoutConstraint *b12 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeRight
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.button2 attribute:NSLayoutAttributeLeft
//                                                         multiplier:1.0 constant:0.0];
//    
//    NSLayoutConstraint *b21 = [NSLayoutConstraint constraintWithItem:self.button2 attribute:NSLayoutAttributeRight
//                                                          relatedBy:NSLayoutRelationEqual
//                                                             toItem:self.view attribute:NSLayoutAttributeRight
//                                                         multiplier:1.0 constant:0.0];
//
//    NSLayoutConstraint *b13 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterY
//                                                           relatedBy:NSLayoutRelationEqual
//                                                              toItem:self.view attribute:NSLayoutAttributeCenterY
//                                                          multiplier:1.0 constant:0.0];
//
//    NSLayoutConstraint *b23 = [NSLayoutConstraint constraintWithItem:self.button2 attribute:NSLayoutAttributeCenterY
//                                                           relatedBy:NSLayoutRelationEqual
//                                                              toItem:self.view attribute:NSLayoutAttributeCenterY
//                                                          multiplier:1.0 constant:0.0];
//    
//    
//    
//    
//    NSLayoutConstraint *bw21 = [NSLayoutConstraint constraintWithItem:self.button2 attribute:NSLayoutAttributeWidth
//                                                           relatedBy:NSLayoutRelationGreaterThanOrEqual
//                                                              toItem:nil attribute:NSLayoutAttributeNotAnAttribute
//                                                          multiplier:1.0 constant:10.0];
//    bw21.priority = UILayoutPriorityDefaultHigh;
//    
//    NSLayoutConstraint *bw22 = [NSLayoutConstraint constraintWithItem:self.button2 attribute:NSLayoutAttributeWidth
//                                                           relatedBy:NSLayoutRelationLessThanOrEqual
//                                                              toItem:nil attribute:NSLayoutAttributeNotAnAttribute
//                                                          multiplier:1.0 constant:10.0];
//    
//    bw22.priority = UILayoutPriorityDefaultLow;
// 
//    NSLayoutConstraint *bw11 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeWidth
//                                                            relatedBy:NSLayoutRelationGreaterThanOrEqual
//                                                               toItem:nil attribute:NSLayoutAttributeNotAnAttribute
//                                                           multiplier:1.0 constant:10.0];
//    bw11.priority = UILayoutPriorityDefaultHigh+1;
//    
//    NSLayoutConstraint *bw12 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeWidth
//                                                            relatedBy:NSLayoutRelationLessThanOrEqual
//                                                               toItem:nil attribute:NSLayoutAttributeNotAnAttribute
//                                                           multiplier:1.0 constant:10.0];
//    
//    bw12.priority = UILayoutPriorityDefaultLow;
//    
//    [self.view addConstraints:@[ bw11, bw12, bw21,bw22 ]];
//
//
//    NSArray *ca = @[b11,b12,b13, b21, b23];
//    for(NSLayoutConstraint *c in ca) {
//        c.priority = UILayoutPriorityDefaultHigh+5;
//    }
    
    
//    b11.priority = UILayoutPriorityDefaultLow+1;
//    b12.priority = UILayoutPriorityRequired;
    //b21.priority = UILayoutPriorityRequired;
    
//    [self.view addConstraints:ca];
    
//    [self.button2 setContentHuggingPriority:200 forAxis:UILayoutConstraintAxisHorizontal];
//    [self.button2 setContentHuggingPriority:251 forAxis:UILayoutConstraintAxisHorizontal];
//    //[self.button2 setContentCompressionResistancePriority:1000 forAxis:UILayoutConstraintAxisHorizontal];
//    //[self.button1 setContentCompressionResistancePriority:750 forAxis:UILayoutConstraintAxisHorizontal];
//
    //set layout for container
    NSLayoutConstraint *c2 = [NSLayoutConstraint constraintWithItem:self.label1 attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0 constant:0.0];
    NSArray *labelArr = @[
                        @"V:|-0@101-[_label1]-0-[_container]-(>=0@1000)-|",
                        @"[_label1(==300@250)]"
                        ];

    [self.label1 setContentHuggingPriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    for(NSString *s in labelArr) {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:s
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_label1,_container)]];
    }

    
    
    
    NSArray *strArr = @[
                        @"|-0@102-[_button1]-0-[_button2]-0@103-|",
                                                @"V:|-0@102-[_button1]-0-|",
                        @"V:|-0@102-[_button2]-0-|",
                        ];
    
    for(NSString *s in strArr) {
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:s
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(_button1,_button2,_container)]];
    }
    
    NSLayoutConstraint *c1 = [NSLayoutConstraint constraintWithItem:self.container attribute:NSLayoutAttributeCenterX
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self.view attribute:NSLayoutAttributeCenterX
                                                          multiplier:1.0 constant:0.0];
    
    [self.view addConstraints:@[ c1, c2 ]];

}

- (IBAction)oneAction:(id)sender {
    [self.button1 setTitle:@"with great power" forState:UIControlStateNormal];
    self.label1.text = @"lorem alsdj flakjsd flkajd flkaj sdlfjalsd";
}
- (IBAction)twoAction:(id)sender {
    [self.button2 setTitle:@"comes great layouting!" forState:UIControlStateNormal];
    self.label1.text = @"lorem alsdj flakjsd flkajd flkaj sdlfjalsd flakjsd flkajd flkaj sdlfjalsd flakjsd flkajd flkaj sdlfjalsd flakjsd flkajd flkaj sdlfjalsd flakjsd flkajd flkaj sdlfjalsd";


}


@end

/*
 1.
 
 [self.view addConstraints:@[
 [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterX
 relatedBy:NSLayoutRelationEqual
 toItem:self.view attribute:NSLayoutAttributeCenterX
 multiplier:1.0 constant:0.0]
 ,
 [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterY
 relatedBy:NSLayoutRelationEqual
 toItem:self.view attribute:NSLayoutAttributeCenterY
 multiplier:1.0 constant:0.0]
 ]
 
 
 2.
 [self.view addConstraints:@[
 [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeLeft
 relatedBy:NSLayoutRelationEqual
 toItem:self.view attribute:NSLayoutAttributeLeading
 multiplier:1.0 constant:0.0]
 ,
 [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterY
 relatedBy:NSLayoutRelationEqual
 toItem:self.view attribute:NSLayoutAttributeCenterY
 multiplier:1.0 constant:0.0]
 ]
 ];

*/