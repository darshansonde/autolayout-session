//
//  NineViewController.m
//  autolayouttest
//
//  Created by Darshan Sonde on 10/15/14.
//  Copyright (c) 2014 Darshan Sonde. All rights reserved.
//

#import "NineViewController.h"

// Animation

@interface NineViewController ()
@property (nonatomic, strong) UIButton *button1;
@property (nonatomic, strong) UIButton *button2;
@property (nonatomic, strong) UILabel  *label1;

@property (nonatomic, strong) UIView *spacer1;
@property (nonatomic, strong) UIView *spacer2;
@property (nonatomic, strong) UIView *spacer3;
@property (nonatomic, strong) NSLayoutConstraint *cons;
@end

@implementation NineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];//create the button and add as subview
    [self setupConstraints];
}


-(void) setupConstraints {
    NSDictionary *varDict  = NSDictionaryOfVariableBindings(_button1,_button2);
    NSArray *consArray = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_button1]-0-[_button2]"
                                                                 options:0 metrics:nil
                                                                   views:varDict];
    [self.view addConstraints:consArray];
    
    NSArray *border = [NSLayoutConstraint constraintsWithVisualFormat:@"[_button2]-(>=0)-|"
                                                              options:0 metrics:nil
                                                                views:varDict];
    [self.view addConstraints:border];
    
    //equal buttons
    NSLayoutConstraint *eq = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.button2 attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0 constant:0.0];
    eq.priority = 500;
    [self.view addConstraint:eq];
    
    NSLayoutConstraint *l2 = [NSLayoutConstraint constraintWithItem:self.label1 attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0 constant:0.0];
    [self.view addConstraint:l2];
    
    
    self.label1.preferredMaxLayoutWidth = 200.0;//multiline text
    
    
    //y constraints
    /* Remove Y Constraints.
     NSLayoutConstraint *y1 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterY
     relatedBy:NSLayoutRelationEqual
     toItem:self.view attribute:NSLayoutAttributeCenterY
     multiplier:1.0 constant:0.0];
     [self.view addConstraint:y1];
     
     NSLayoutConstraint *y2 = [NSLayoutConstraint constraintWithItem:self.button2 attribute:NSLayoutAttributeCenterY
     relatedBy:NSLayoutRelationEqual
     toItem:self.view attribute:NSLayoutAttributeCenterY
     multiplier:1.0 constant:0.0];
     [self.view addConstraint:y2];
     
     NSLayoutConstraint *l1 = [NSLayoutConstraint constraintWithItem:self.label1 attribute:NSLayoutAttributeTop
     relatedBy:NSLayoutRelationEqual
     toItem:self.view attribute:NSLayoutAttributeTop
     multiplier:1.0 constant:20.0];
     [self.view addConstraint:l1];*/
    
    
    
    
    
    
    
    //step1, y and height for spacers
    NSDictionary *yDict = NSDictionaryOfVariableBindings(_button1,_button2,_label1,_spacer1,_spacer2,_spacer3);
    
    NSArray *yConstraints = [NSLayoutConstraint constraintsWithVisualFormat:
                             @"V:|-0-[_spacer1]-0-[_label1]-0-[_spacer2(==_spacer1)]-0-[_button1]-0-[_spacer3(==_spacer1)]-0-|"
                                                                    options:0 metrics:nil
                                                                      views:yDict];
    [self.view addConstraints:yConstraints];
    
    NSArray *yConstraints2 = [NSLayoutConstraint constraintsWithVisualFormat:
                              @"V:[_spacer2]-0-[_button2]"
                                                                     options:0 metrics:nil
                                                                       views:yDict];
    [self.view addConstraints:yConstraints2];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //x for spacers
    NSLayoutConstraint *spacer1x = [NSLayoutConstraint constraintWithItem:self.spacer1 attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.view attribute:NSLayoutAttributeCenterX
                                                               multiplier:1.0 constant:0.0];
    [self.view addConstraint:spacer1x];
    NSLayoutConstraint *spacer2x = [NSLayoutConstraint constraintWithItem:self.spacer2 attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeLeft
                                                               multiplier:1.0 constant:0.0];
    [self.view addConstraint:spacer2x];
    NSLayoutConstraint *spacer3x = [NSLayoutConstraint constraintWithItem:self.spacer3 attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeLeft
                                                               multiplier:1.0 constant:0.0];
    [self.view addConstraint:spacer3x];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //width for spacers
    NSLayoutConstraint *spacer1w = [NSLayoutConstraint constraintWithItem:self.spacer1 attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:0.0 constant:100.0];
    [self.view addConstraint:spacer1w];
    NSLayoutConstraint *spacer2w = [NSLayoutConstraint constraintWithItem:self.spacer2 attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeWidth
                                                               multiplier:0.0 constant:100.0];
    [self.view addConstraint:spacer2w];
    NSLayoutConstraint *spacer3w = [NSLayoutConstraint constraintWithItem:self.spacer3 attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeWidth
                                                               multiplier:0.0 constant:100.0];
    [self.view addConstraint:spacer3w];
    
    
    
}



















































-(void) button1Action:(UIButton*)sender {
    [self.button1 setTitle:@"Hello Eq World!" forState:UIControlStateNormal];
//    self.label1.text = @"Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World!";
    NSLayoutConstraint *w = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeWidth
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.view attribute:NSLayoutAttributeWidth
                                                        multiplier:0.5 constant:0.0];
    [self.view addConstraint:w];
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(void) button2Action:(UIButton*)sender {
    [self.button2 setTitle:@"Hello Autolayout World!" forState:UIControlStateNormal];
    self.label1.text = @"Hello Autolayout WorldHello Autolayout WorldHello Autolayout WorldHello Autolayout WorldHello Autolayout!";
}























-(void) loadView {
    self.view = ({
        UIView *v = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        v.backgroundColor = [UIColor whiteColor];
        v.translatesAutoresizingMaskIntoConstraints = NO;
        v;
    });
}

-(void) setupUI {
    self.button1 = ({
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setTitle:@"Hello" forState:UIControlStateNormal];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor blueColor];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:b];
        b;
    });
    
    self.button2 = ({
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setTitle:@"Hello" forState:UIControlStateNormal];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor magentaColor];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:b];
        b;
    });
    
    self.label1 = ({
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectZero];
        l.translatesAutoresizingMaskIntoConstraints = NO;
        l.layer.borderColor = [UIColor cyanColor].CGColor;
        l.layer.borderWidth = 2.0f;
        l.numberOfLines=0;
        l.text = @"Lorem ipsum dolor sit amet";
        [self.view addSubview:l];
        l;
    });
    
    self.spacer1 = ({
        UIView *v = [[UIView alloc ]initWithFrame:CGRectZero];
        v.translatesAutoresizingMaskIntoConstraints = NO;
        v.layer.borderColor = [UIColor blueColor].CGColor;
        v.layer.borderWidth = 1.0f;
        v.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:v];
        v;
    });
    
    self.spacer2 = ({
        UIView *v = [[UIView alloc ]initWithFrame:CGRectZero];
        v.translatesAutoresizingMaskIntoConstraints = NO;
        v.layer.borderColor = [UIColor blueColor].CGColor;
        v.layer.borderWidth = 1.0f;
        v.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:v];
        v;
    });
    self.spacer3 = ({
        UIView *v = [[UIView alloc ]initWithFrame:CGRectZero];
        v.translatesAutoresizingMaskIntoConstraints = NO;
        v.layer.borderColor = [UIColor blueColor].CGColor;
        v.layer.borderWidth = 1.0f;
        v.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:v];
        v;
    });
    
}

@end
