//
//  AppDelegate.m
//  autolayouttest
//
//  Created by Darshan Sonde on 02/07/14.
//  Copyright (c) 2014 Darshan Sonde. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //[self oneViewController];
    //[self twoViewController];
    //[self threeViewController];
    //[self fourViewController];
    //[self fourViewController];
    //[self fiveViewController];
    //[self sixViewController];
    //[self sevenViewController];
    //[self eightViewController];
    [self nineViewController];
    
    [self.window makeKeyAndVisible];
    return YES;
}























-(void) oneViewController
{
    OneViewController *vc = [[OneViewController alloc] init];
    self.window.rootViewController = vc;
}

-(void) twoViewController
{
    TwoViewController *vc = [[TwoViewController alloc] init];
    self.window.rootViewController = vc;
}
-(void) threeViewController
{
    ThreeViewController *vc = [[ThreeViewController alloc] init];
    self.window.rootViewController = vc;
}
-(void) fourViewController
{
    FourViewController *vc = [[FourViewController alloc] init];
    self.window.rootViewController = vc;
}
-(void) fiveViewController
{
    FiveViewController *vc = [[FiveViewController alloc] init];
    self.window.rootViewController = vc;
}
-(void) sixViewController
{
    SixViewController *vc = [[SixViewController alloc] init];
    self.window.rootViewController = vc;
}
-(void) sevenViewController
{
    SevenViewController *vc = [[SevenViewController alloc] init];
    self.window.rootViewController = vc;
}
-(void) eightViewController
{
    EightViewController *vc = [[EightViewController alloc] initWithNibName:@"EightViewController" bundle:nil];
//        EightViewController *vc = [[EightViewController alloc] initWithNibName:@"EightViewController_WithConstraints" bundle:nil];
    self.window.rootViewController = vc;
}
-(void) nineViewController
{
    NineViewController *vc = [[NineViewController alloc] init];
    self.window.rootViewController = vc;
}



-(void) viewController
{
    ViewController *vc = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    self.window.rootViewController = vc;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
