//
//  SixViewController.m
//  autolayouttest
//
//  Created by Darshan Sonde on 10/14/14.
//  Copyright (c) 2014 Darshan Sonde. All rights reserved.
//

#import "SixViewController.h"

@interface SixViewController ()
@property (nonatomic, strong) UIButton *button1;
@property (nonatomic, strong) UIButton *button2;
@property (nonatomic, strong) UILabel  *label1;
@end

@implementation SixViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    [self setupConstraints];
}


-(void) setupConstraints {
    NSDictionary *varDict  = NSDictionaryOfVariableBindings(_button1,_button2);
    NSArray *consArray = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_button1]-0-[_button2]"
                                                                 options:0 metrics:nil
                                                                   views:varDict];
    [self.view addConstraints:consArray];
    
    NSArray *border = [NSLayoutConstraint constraintsWithVisualFormat:@"[_button2]-(>=0)-|"
                                                              options:0 metrics:nil
                                                                views:varDict];
    [self.view addConstraints:border];

    //equal buttons
    NSLayoutConstraint *eq = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.button2 attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0 constant:0.0];
    [self.view addConstraint:eq];

    
    NSLayoutConstraint *y1 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0 constant:0.0];
    [self.view addConstraint:y1];
    
    NSLayoutConstraint *y2 = [NSLayoutConstraint constraintWithItem:self.button2 attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0 constant:0.0];
    [self.view addConstraint:y2];
    
    
    
    //label constraints
    
    //step 1, label constraints
    
    //step 2
    
    //step 3
    //step 4

    //step 5
    // step 6, compression resistance
}





















/*
 step 1
 NSLayoutConstraint *l1 = [NSLayoutConstraint constraintWithItem:self.label1 attribute:NSLayoutAttributeTop
 relatedBy:NSLayoutRelationEqual
 toItem:self.view attribute:NSLayoutAttributeTop
 multiplier:1.0 constant:0.0];
 [self.view addConstraint:l1];
 
 NSLayoutConstraint *l2 = [NSLayoutConstraint constraintWithItem:self.label1 attribute:NSLayoutAttributeCenterX
 relatedBy:NSLayoutRelationEqual
 toItem:self.view attribute:NSLayoutAttributeCenterX
 multiplier:1.0 constant:0.0];
 [self.view addConstraint:l2];

 
 
 

 
 
 
 
 
 
 
 step 2, top constant
 constant:20.0
 
 
 
 
 
 
 
 
 
 
 
 
 step 3
 self.label1.preferredMaxLayoutWidth = 200.0;//multiline text
 
 
 
 
 step 4
 NSLayoutConstraint *l3 = [NSLayoutConstraint constraintWithItem:self.label1 attribute:NSLayoutAttributeHeight
 relatedBy:NSLayoutRelationLessThanOrEqual
 toItem:nil attribute:NSLayoutAttributeNotAnAttribute
 multiplier:0.0 constant:75.0];
 [self.view addConstraint:l3];
 
 
 step 5
 self.label1.adjustsFontSizeToFitWidth = YES;
 
 
 step 6, compression resistance
 l3.priority = 740;
 */






-(void) button1Action:(UIButton*)sender {
    [self.button1 setTitle:@"Hello Eq World!" forState:UIControlStateNormal];
    self.label1.text = @"Hello Eq World!";
}
-(void) button2Action:(UIButton*)sender {
    [self.button2 setTitle:@"Hello Autolayout World!" forState:UIControlStateNormal];
        self.label1.text = @"Hello Autolayout WorldHello Autolayout WorldHello Autolayout WorldHello Autolayout WorldHello Autolayout!";
}

















-(void) loadView {
    self.view = ({
        UIView *v = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        v.backgroundColor = [UIColor whiteColor];
        v.translatesAutoresizingMaskIntoConstraints = NO;
        v;
    });
}

-(void) setupUI {
    self.button1 = ({
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setTitle:@"Hello" forState:UIControlStateNormal];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor blueColor];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:b];
        b;
    });
    
    self.button2 = ({
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setTitle:@"Hello" forState:UIControlStateNormal];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor magentaColor];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:b];
        b;
    });
    
    self.label1 = ({
        UILabel *l = [[UILabel alloc] initWithFrame:CGRectZero];
        l.translatesAutoresizingMaskIntoConstraints = NO;
        l.layer.borderColor = [UIColor cyanColor].CGColor;
        l.layer.borderWidth = 2.0f;
        l.numberOfLines=0;
        l.text = @"Lorem ipsum dolor sit amet";
        [self.view addSubview:l];
        l;
    });
}



@end
